﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using Seleniumwork.Configuration;
using Seleniumwork.CustomException;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.BaseClass
{
    [TestClass]
    public class TestBase
    {
        public static IWebDriver driver;
        private static IWebDriver GetChromeDriver()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            return driver;
        }
        private static IWebDriver GetFirfoxDriver()
        {
             driver = new FirefoxDriver();
            driver.Manage().Window.Maximize();
            return driver;

        }
        private static IWebDriver GetIEDriver()
        {
             driver = new InternetExplorerDriver();
            driver.Manage().Window.Maximize();
            return driver;
        }

        

        [AssemblyInitialize]
        public static void InitWebdriver(TestContext tc)
        {
            ObjectRepository.Config = new AppConfigReader();

            switch (ObjectRepository.Config.GetBrowser())
            {
                case BrowserType.Firefox:
                    ObjectRepository.Driver = GetFirfoxDriver();
                    
                    break;
                case BrowserType.Chrome:
                    ObjectRepository.Driver = GetChromeDriver();
                                                            
                    break;
                case BrowserType.IExplorer:
                    ObjectRepository.Driver = GetIEDriver();
                   
                    break;
                default:
                    throw new NoSuitableDriverFound("Driver not found: " + ObjectRepository.Config.GetBrowser().ToString());
            }
        }



        /*
        [AssemblyCleanup]

        
        public static void TearDown()
        {
            if(ObjectRepository.Driver != null)
            {
                //ObjectRepository.Driver.Close();
                ObjectRepository.Driver.Quit();
            }
        }*/

    }
}
