﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Seleniumwork.CustomException;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.ComponentHelper
{
    public class LinkHelper
    {
        private static IWebElement element;

        public static void ClickLink(By locator)
        {
            try
            {

                element = GenericHelper.GetElement(locator);
                element.Click();

            }catch(StaleElementReferenceException)
            {
                
                Console.WriteLine("Locator not found, so unable to click..");
                AssertFailedException();
            }
        }



        private static void AssertFailedException()
        {
            throw new ElementNotFoundException("Element not found! ");
        }


        public static bool retryingFindClick(By locator)
        {
            bool result = false;
            int attempts = 0;
            while (attempts < 10)
            {
                try
                {
                    GenericHelper.GetElement(locator).Click();
                    result = true;
                    break;
                }
                catch (StaleElementReferenceException e)
                {
                    Console.WriteLine("Locator not found, so unable to click...rfc");
                   // AssertFailedException();
                }
                attempts++;
            }
            return result;
        }


    }
}
