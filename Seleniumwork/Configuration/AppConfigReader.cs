﻿using Seleniumwork.Interfaces;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.Configuration
{
    public class AppConfigReader : IConfig
    {
        public string Getwebsite()
        {
            return ConfigurationManager.AppSettings.Get(AppConfigKeys.Website);
        }
        public BrowserType GetBrowser()
        {
            string browser = ConfigurationManager.AppSettings.Get(AppConfigKeys.Browser);
            return (BrowserType)Enum.Parse(typeof(BrowserType), browser);
        }
        public string GetUserName()
        {
            return ConfigurationManager.AppSettings.Get(AppConfigKeys.Username);
        }
        public string GetPassword()
        {
            return ConfigurationManager.AppSettings.Get(AppConfigKeys.Password);
        }
              
       
    }
}
