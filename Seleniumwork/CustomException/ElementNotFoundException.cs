﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.CustomException
{
    
    public class ElementNotFoundException : Exception
    {
        public ElementNotFoundException(string msg) : base(msg)
        {

        }

    }
}
