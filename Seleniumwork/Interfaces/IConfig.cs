﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.Interfaces
{
    public interface IConfig
    {
        Configuration.BrowserType GetBrowser();
        string Getwebsite();
        string GetUserName();
        string GetPassword();
        
    }
}
