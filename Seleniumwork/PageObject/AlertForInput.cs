﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Seleniumwork.BaseClass;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Seleniumwork.PageObject
{
    public class AlertForInput : TestBase
    {
        public void EnterName(string message, string name)
        {
            OpenQA.Selenium.IAlert alert = ObjectRepository.Driver.SwitchTo().Alert();
            if (alert.Text.Equals(message))
            {
                Console.WriteLine("Alert box display expected message: " + message);
                Thread.Sleep(2000);
                alert.SendKeys(name);
                Thread.Sleep(2000);
                alert.Accept();
                Console.WriteLine("Accepted the Alert.");
            }
            else
            {
                Assert.Fail("String specified " + message + " cannot be found ..");
            }
        }




    }
}
