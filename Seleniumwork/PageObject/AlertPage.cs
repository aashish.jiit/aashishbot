﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using Seleniumwork.BaseClass;
using Seleniumwork.ComponentHelper;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.PageObject
{
    public class AlertPage : TestBase
    {

        //POM

        #region WebElement  


        private By simpleAlert = By.XPath("//a[contains(text(),'Simple Alert')]");
        private By inputAlert = By.XPath("//a[contains(text(),'Input Alert')]");
        private By iframeForSimpleAlert = By.XPath("//div[@id='example-1-tab-1']//iframe[@class='demo-frame']");
        private By iframeForInputAlert = By.XPath("//div[@id='example-1-tab-2']//iframe[@class='demo-frame']");
        private By buttonToDisplayAlertBox = By.XPath("//button[@onclick='myFunction()']");
        private By buttonToDisplayInputAlertBox = By.XPath("//button[@onclick='myFunction()']");
        private By textToValidate = By.XPath("//p[@id='demo']");

        public Alert Alert = new Alert();
        public AlertForInput AlertForInput = new AlertForInput();

        #endregion



        #region Action

        public void Click(string str)
        {
            switch (str.ToUpper().Trim())
            {
                case "SIMPLE ALERT":


                    if (GenericHelper.IsElementPresent(simpleAlert))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        LinkHelper.ClickLink(simpleAlert);

                    }
                    else
                    {
                        Console.WriteLine("Simple Alert Element Not present");
                    }
                    break;

                case "INPUT ALERT":


                    if (GenericHelper.IsElementPresent(simpleAlert))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        LinkHelper.ClickLink(inputAlert);

                    }
                    else
                    {
                        Console.WriteLine("Input Alert Element Not present");
                    }
                    break;


                case "CLICK THE BUTTON TO DISPLAY AN ALERT BOX:":


                    ObjectRepository.Driver.SwitchTo().Frame(ObjectRepository.Driver.FindElement(iframeForSimpleAlert));

                    if (GenericHelper.IsElementPresent(buttonToDisplayAlertBox))
                    {


                        // dropdownSelect locator for the dropdown and str is option to select
                        ButtonHelper.ClickButton(buttonToDisplayAlertBox);
                        

                    }
                    else
                    {
                        throw new NoSuchElementException("Element not found! " + buttonToDisplayAlertBox.ToString());
                    }
                    break;

                case "CLICK THE BUTTON TO DEMONSTRATE THE INPUT BOX.":


                    ObjectRepository.Driver.SwitchTo().Frame(ObjectRepository.Driver.FindElement(iframeForInputAlert));

                    if (GenericHelper.IsElementPresent(buttonToDisplayInputAlertBox))
                    {


                        // dropdownSelect locator for the dropdown and str is option to select
                        ButtonHelper.ClickButton(buttonToDisplayInputAlertBox);


                    }
                    else
                    {
                        throw new NoSuchElementException("Element not found! " + buttonToDisplayInputAlertBox.ToString());
                    }
                    break;


                default:

                    Assert.Fail("String specified " + str + " cannot be found ..");


                    break;

            }

            
        }
        public void Validate(string name)
        {
            string check = "Hello " + name + "! How are you today?";
            try {

                Assert.IsTrue(ObjectRepository.Driver.FindElement(textToValidate).Text.Equals(check));
                Console.WriteLine("String on board matched!!");
                    }
            catch (Exception)
            {
                
                Console.WriteLine("string does not match with what is on board..");
                Assert.Fail("Expected String cannot be found ..");
            }
        }
        #endregion

    }
}
