﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Seleniumwork.BaseClass;
using Seleniumwork.ComponentHelper;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Seleniumwork.PageObject
{
    public class EnterCountry : TestBase
    {

        //POM

        #region WebElement  


        private By enterCountry = By.XPath("//a[contains(text(),'Enter Country')]");
        private By iframe2 = By.XPath("//div[@id='example-1-tab-2']//iframe[@class='demo-frame']");
        private By entercountrycombobox = By.ClassName("ui-button-text");
        private By countryList = By.XPath("//body/div[1]/span[1]/input[1]");
        private By dropbox = By.XPath("//input[contains(@class,'ui-widget-content')]");
        private By suggestionbox = By.XPath("//ul[contains(@style,'block')]");

        #endregion



        #region Action

        public void EnterCountryName(string cname)
        {
            ObjectRepository.Driver.SwitchTo().Frame(ObjectRepository.Driver.FindElement(iframe2));

            if (GenericHelper.IsElementPresent(entercountrycombobox))
            {

                try
                {
                    ObjectRepository.Driver.FindElement(entercountrycombobox).Click();
                    ObjectRepository.Driver.FindElement(dropbox).Clear();
                    ObjectRepository.Driver.FindElement(dropbox).SendKeys(cname);

                    if (GenericHelper.IsElementPresent(suggestionbox))
                    {


                        Thread.Sleep(2000);

                        ObjectRepository.Driver.FindElement(dropbox).SendKeys(Keys.Down);
                        Thread.Sleep(2000);
                        ObjectRepository.Driver.FindElement(dropbox).SendKeys(Keys.Enter);
                        Console.WriteLine("Wahh!!!!");
                    }
                    else
                    {
                        Assert.Fail("String specified " + cname + " cannot be found ..");
                    }
                    
                }
                catch(Exception)
                    {
                    throw new NoSuchElementException("String not found in dropdown! " + entercountrycombobox.ToString());
                }
                // }


            }
            else
            {
                throw new NoSuchElementException("Element not found! " + entercountrycombobox.ToString());
            }
        }

        public void Select(string str)
        {
            switch (str.ToUpper().Trim())
            {
                

                case "ENTER COUNTRY":


                    if (GenericHelper.IsElementPresent(enterCountry))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        LinkHelper.ClickLink(enterCountry);

                    }
                    else
                    {
                        Console.WriteLine("Element Not present");
                    }
                    break;

               
                    
                  


                default:

                    Assert.Fail("String specified " + str + " cannot be found ..");


                    break;

            }

            
        }
        #endregion

    }
}
