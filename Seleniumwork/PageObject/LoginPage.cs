﻿using OpenQA.Selenium;
using Seleniumwork.BaseClass;
using Seleniumwork.ComponentHelper;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.PageObject
{
    public class LoginPage : TestBase
    {

        //POM

        #region WebElement  


        private By LoginTextBox = By.XPath("//div[@id='login']//input[@type='text' and @name = 'username']");
        private By PassTextBox = By.XPath("//div[@id='login']//input[@type='password' and @name = 'password']");
        private By SubmitButton = By.XPath("//div[@id='login']//input[@type='submit']");

        #endregion



        #region Action

        public void Login()
        {
            
            TextBoxHelper.TypeInTextBox(LoginTextBox,ObjectRepository.Config.GetUserName());
            TextBoxHelper.TypeInTextBox(PassTextBox,ObjectRepository.Config.GetPassword());
            ButtonHelper.ClickButton(SubmitButton);

        }

        #endregion


    }
}
