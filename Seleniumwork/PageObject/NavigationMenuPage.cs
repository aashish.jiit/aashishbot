﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using Seleniumwork.BaseClass;
using Seleniumwork.ComponentHelper;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.PageObject
{
    public class NavigationMenuPage : TestBase
    {

        //POM
        
        #region WebElement  


        private By dynamicElement = By.XPath("//a[contains(text(),'Dynamic Elements')]");
        private By dropDown = By.XPath("//a[contains(text(),'Dropdown')]");
        private By alertLink = By.XPath("//a[contains(text(),'Alert')]");
        private By registrationTab = By.XPath("//a[contains(text(),'Registration')]");

        #endregion



        #region Action

        public void Click(string str)
        {
            switch (str.ToUpper().Trim())
            {
                case "DYNAMIC ELEMENTS":
   
                    if (GenericHelper.IsElementPresent(dynamicElement))
                    {
                        
                      // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        LinkHelper.retryingFindClick(dynamicElement);
                        
                    }
                    else
                    {
                        Console.WriteLine("DE Element Not present");
                    }
                    break;


                case "ALERT":


                    if (GenericHelper.IsElementPresent(alertLink))
                    {
                        
                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        LinkHelper.retryingFindClick(alertLink);

                    }
                    else
                    {
                        Console.WriteLine("DE Element Not present");
                    }
                    break;

                case "DROPDOWN":


                    if (GenericHelper.IsElementPresent(dropDown))
                    {
                        Console.WriteLine("DD Present");
                        //ObjectRepository.Driver.FindElement(SignInLink).Click();
                        WebDriverWait wait = new WebDriverWait(TestBase.driver, TimeSpan.FromSeconds(10));
                        wait.Until(OpenQA.Selenium.Support.UI.ExpectedConditions.ElementToBeClickable(dropDown));
                        LinkHelper.ClickLink(dropDown);
                       

                    }
                    else
                    {
                        Console.WriteLine("DD Element Not present");
                    }
                    break;

                case "REGISTRATION":


                    if (GenericHelper.IsElementPresent(registrationTab))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        LinkHelper.retryingFindClick(registrationTab);
                        //LinkHelper.ClickLink(registrationTab);

                    }
                    else
                    {
                        Console.WriteLine("Registration tab Not present on page");
                    }
                    break;

                default:
                    Console.WriteLine("Please check if the element is spelled correctly..");
                     
                    break;

            }
        }

        #endregion

    }
}
