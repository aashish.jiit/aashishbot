﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using Seleniumwork.BaseClass;
using Seleniumwork.ComponentHelper;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.PageObject
{
    public class RegistrationPage : TestBase
    {
        
       
        //POM

        #region WebElement  

        //[FindsBy(How = How.XPath, Using = "//a[contains(text(),'Signin')]")]
        //IWebElement SigninLink;

        private By SignInLink =  By.XPath("//a[contains(text(),'Signin')]");

        #endregion
                


        #region Action
        
        public  void Click(string str)
        {
            switch (str.ToUpper().Trim())
            {
                case "SIGNIN":
                    

                    if (GenericHelper.IsElementPresent(SignInLink))
                    {
                        //ObjectRepository.Driver.FindElement(SignInLink).Click();
                        LinkHelper.ClickLink(SignInLink);
                        
                    }
                    else
                    {
                        Console.WriteLine("Element Not present");
                    }
                    break;

                default:
                    Console.WriteLine("Please check if the element is spelled correctly..");
                    
                    break;

            }
        }

        #endregion

    }
}
