﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using Seleniumwork.BaseClass;
using Seleniumwork.ComponentHelper;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.PageObject
{
      public class RegistrationTab : TestBase
    {
        //POM

        #region WebElement  


        
        private By firstNameInput = By.XPath("//input[@type='text' and @name ='name']");
        private By lastNameInput = By.XPath("//label[contains(text(),'Last Name')]/following-sibling::input[1]");
        private By hobbyDance = By.XPath("//label[contains(text(),' Dance')]");
        private By hobbyReading = By.XPath("//label[contains(text(),' Reading')]");
        private By hobbyCricket = By.XPath("//label[contains(text(),'Cricket')]");
        private By phoneNumber = By.XPath("//input[@type='text' and @name='phone']");
        private By userName = By.XPath("//input[@type='text' and @name='username']");
        private By email = By.XPath("//input[@type='text' and @name='email']");
        private By password = By.XPath("//input[@type='password' and @name='password']");
        private By confirmPassword = By.XPath("//input[@type='password' and @name='c_password']");
        private By submitButton = By.XPath("//input[@type='submit' and @value='submit']");
        private By registrationPanel = By.XPath("//div[@class='registration_form']/h2[contains(text(),'Registration Form')]");

        #endregion



        #region Action

        public void Set(string field,string value)
        {
            switch (field.ToUpper().Trim())
            {
                case "FIRST NAME:":


                    if (GenericHelper.IsElementPresent(firstNameInput))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        TextBoxHelper.TypeInTextBox(firstNameInput, value);

                    }
                    else
                    {
                        Console.WriteLine(field + " field not present on page");
                    }
                    break;

                

                

                case "LAST NAME:":


                    if (GenericHelper.IsElementPresent(lastNameInput))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        TextBoxHelper.TypeInTextBox(lastNameInput, value);

                    }
                    else
                    {
                        Console.WriteLine(field + " field not present on page");
                    }
                    break;

                case "HOBBY:":

                    {
                        if (value == "Dance")
                        {

                            ObjectRepository.Driver.FindElement(hobbyDance).Click();
                        }
                        else if (value == "Reading")
                        {

                            ObjectRepository.Driver.FindElement(hobbyReading).Click();
                        }
                        else if (value == "Cricket")
                        {

                            ObjectRepository.Driver.FindElement(hobbyCricket).Click();
                        }
                        else
                        {
                            Assert.Fail("Unable to select hobby specified " + value + "  ..");
                        }


                    }
                    break;

                case "PHONE NUMBER:":


                    if (GenericHelper.IsElementPresent(phoneNumber))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        TextBoxHelper.TypeInTextBox(phoneNumber, value);

                    }
                    else
                    {
                        Console.WriteLine(field + " field not present on page");
                    }
                    break;

                case "USERNAME:":


                    if (GenericHelper.IsElementPresent(userName))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        TextBoxHelper.TypeInTextBox(userName, value);

                    }
                    else
                    {
                        Console.WriteLine(field + " field not present on page");
                    }
                    break;

                case "E-MAIL:":


                    if (GenericHelper.IsElementPresent(email))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        TextBoxHelper.TypeInTextBox(email, value);

                    }
                    else
                    {
                        Console.WriteLine(field + " field not present on page");
                    }
                    break;

                    case "PASSWORD:":


                    if (GenericHelper.IsElementPresent(password))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        TextBoxHelper.TypeInTextBox(password, value);

                    }
                    else
                    {
                        Console.WriteLine(field + " field not present on page");
                    }
                    break;

                case "CONFIRM PASSWORD:":


                    if (GenericHelper.IsElementPresent(confirmPassword))
                    {

                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        TextBoxHelper.TypeInTextBox(confirmPassword, value);

                    }
                    else
                    {
                        Console.WriteLine(field + " field not present on page");
                    }
                    break;

                default:

                    Assert.Fail("Field specified " + field + " cannot be found ..");


                    break;

            }


        }
        public void Click(string btn)
        {
            if (btn.ToUpper().Equals("SUBMIT"))
            {
                ButtonHelper.ClickButton(submitButton);
                Console.WriteLine("Submit button clicked successfully!");
            }
            else
            {
                Assert.Fail("Field specified " + btn + " cannot be found ..");
            }
        }

        public void VerifyLandingPage(string lpage)
        {
            if (lpage.ToUpper().Equals("REGISTRATION"))
            {
                if (GenericHelper.IsElementPresent(registrationPanel))
                {
                    Console.WriteLine("Successful landing on Registration Page!");

                }
                else
                {
                    Assert.Fail("Element not found on webpage. Function Update may be required " + lpage + " ..");
                }
            }
            else
            {
                Assert.Fail("Landing Page not as specified " + lpage + "Function Update may be required ..");
            }
        }



        #endregion

    }
}

