﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using Seleniumwork.BaseClass;
using Seleniumwork.ComponentHelper;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.PageObject
{
    public class SelectCountry    : TestBase
    {

        //POM
        
        #region WebElement  


        private By selectCountry = By.XPath("//a[contains(text(),'Select Country')]");
        private By iframe = By.XPath("//div[@id='example-1-tab-1']//iframe[@class='demo-frame']");
        private By dropdownSelect = By.TagName("select");
        private By dropdownOption = By.XPath("//option[contains(text(),'Ghana')]");

        


        #endregion



        #region Action

        public void Select(string str)
        {
            switch (str.ToUpper().Trim())
            {
                case "SELECT COUNTRY":


                    if (GenericHelper.IsElementPresent(selectCountry))
                    {
                        
                        // ObjectRepository.Driver.FindElement(dynamicElement).Click();
                        LinkHelper.ClickLink(selectCountry);

                    }
                    else
                    {
                        Console.WriteLine("Element Not present");
                    }
                    break;


                case "GHANA" :

                    
                    ObjectRepository.Driver.SwitchTo().Frame(ObjectRepository.Driver.FindElement(iframe));
                    if (GenericHelper.IsElementPresent(dropdownSelect))
                    {

                        
                        // dropdownSelect locator for the dropdown and str is option to select
                        DropDownHelper.SelectElement(dropdownSelect, str);

                    }
                    else
                    {
                        throw new NoSuchElementException("Element not found! " + dropdownOption.ToString());
                    }
                    break;

                

                default:
                   
                        Assert.Fail("String specified " + str + " cannot be found ..");
                    

                    break;

            }
        }
        #endregion

    }
}
