﻿using Seleniumwork.BaseClass;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.PageObject
{
    public class WebUrl : TestBase
    {
        public RegistrationPage RegistrationPage;
        public LoginPage LoginPage;
        public NavigationMenuPage NavigationMenuPage;
        public SelectCountry SelectCountry;
        public EnterCountry EnterCountry;
        public AlertPage AlertPage;
        public RegistrationTab RegistrationTab;


        public WebUrl()
        {
            
            RegistrationPage = new RegistrationPage();
            LoginPage = new LoginPage();
            NavigationMenuPage = new NavigationMenuPage();
            SelectCountry = new SelectCountry();
            EnterCountry = new EnterCountry();
            AlertPage = new AlertPage();
            RegistrationTab = new RegistrationTab();



        }

       
    }
}
