﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Seleniumwork.WorkArea
{
    class DropDown
    {
        IWebDriver driver = new ChromeDriver();
        IWebElement dropDownMenu;
        IWebElement elementDromTheDropDownMenu;

        public void DropD()
        {
            string url = "https://testing.todorvachev.com/special-elements/drop-down-menu-test/";

            string dropDownMenuElement = "#post-6 > div > p:nth-child(6) > select > option:nth-child(3)";

            driver.Navigate().GoToUrl(url);

            dropDownMenu = driver.FindElement(By.Name("DropDownTest"));
            Console.WriteLine("The selected value is " + dropDownMenu.GetAttribute("value"));

            elementDromTheDropDownMenu = driver.FindElement(By.CssSelector(dropDownMenuElement));
            Console.WriteLine("The third value is " + elementDromTheDropDownMenu.GetAttribute("value"));
            elementDromTheDropDownMenu.Click();

            for (int i=1; i<4;i++)
            {
                dropDownMenuElement = "#post-6 > div > p:nth-child(6) > select > option:nth-child(" + i + ")";
                elementDromTheDropDownMenu = driver.FindElement(By.CssSelector(dropDownMenuElement));
                Console.WriteLine("The "+ i +" option from dropdown value is " + elementDromTheDropDownMenu.GetAttribute("value"));
            }

            Console.ReadKey();
            driver.Quit();


        }

    }
}
