﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Seleniumwork.WorkArea
{
   public class FindByID
    {
        public void findById()
        { 
        string url = "https://testing.todorvachev.com/selectors/id/";
        string ID = "testImage";

        IWebDriver driver = new ChromeDriver();

        driver.Navigate().GoToUrl(url);
        //Thread.Sleep(3000);

        IWebElement element = driver.FindElement(By.Id(ID));

            if(element.Displayed)
            {
                GreenMessage("Yes! I can see the element.. right there");
    }
            else
            {
                RedMessage("No I din see it!!");
}


driver.Quit();

        }

        private static void RedMessage(string message)
{
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine(message);
    Console.ForegroundColor = ConsoleColor.White;
}

        private static void GreenMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;

        }



    }
}
