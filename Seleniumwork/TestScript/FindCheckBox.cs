﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Seleniumwork.WorkArea
{
    class FindCheckBox
    {
        public void ByCBox()
        {

            string option = "3";
            string url = "https://testing.todorvachev.com/special-elements/check-button-test-3/";
           
            IWebDriver driver = new ChromeDriver();
            IWebElement checkBox;

            driver.Navigate().GoToUrl(url);


            checkBox = driver.FindElement(By.CssSelector("#post-33 > div > p:nth-child(8) > input[type=checkbox]:nth-child(" + option + ")"));
            
            // for radio button radiobutton.GetAttribute
            if (checkBox.GetAttribute("checked") == "true")
            {
                Console.WriteLine( "Checked...");
                Console.WriteLine(checkBox.GetAttribute("value"));
                // to uncheck
                checkBox.Click();
            }
            else
            {
                Console.WriteLine("Not checked..");
            }
            
           
            Thread.Sleep(3000);

            driver.Quit();

        }
        
    }
}
