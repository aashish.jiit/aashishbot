﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.WorkArea
{
    class FindCssPath
    {

        public void ByCss()
        {
            string url = "https://testing.todorvachev.com/selectors/css-path/";
            string cssPath = "#post-108 > div > figure > img";
            string xPath = "//*[@id=\"post - 108\"]/div/figure/img";

            IWebDriver driver = new ChromeDriver();

            driver.Navigate().GoToUrl(url);

            IWebElement cssPathElement = driver.FindElement(By.CssSelector(cssPath));
            IWebElement xPathElement;

            try
            {
                if (cssPathElement.Displayed)
                {
                    GreenMessage("Yes! I can see the CSS.. right there");
                }
            }
            catch (NoSuchElementException)
            {
                RedMessage("No I din see CSS!!");
            }

            try
            {
                xPathElement = driver.FindElement(By.XPath(xPath));
                if (xPathElement.Displayed)
                {
                    GreenMessage("Yes! I can see the xPath.. right there");
                }
            }
            catch(NoSuchElementException)

            {
                RedMessage("No I din see xPath!!");
            }
            
            
           

        }

        private static void RedMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void GreenMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;

        }


    }
}

