﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Seleniumwork.WorkArea
{
    class InputText
    {

        public void ByText()
        {
            string url = "https://testing.todorvachev.com/special-elements/text-input-field/";
            string name = "username";

            IWebDriver driver = new ChromeDriver();
            IWebElement textBox;

            driver.Navigate().GoToUrl(url);
            

            textBox = driver.FindElement(By.Name(name));
            textBox.SendKeys("Test text");
            Thread.Sleep(3000);

            Console.WriteLine(textBox.GetAttribute("value"));
            Console.WriteLine(textBox.GetAttribute("maxlength"));

            textBox.Clear();
            Thread.Sleep(3000);

            driver.Quit();

        }
    }
}
