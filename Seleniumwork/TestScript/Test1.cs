﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Seleniumwork.ComponentHelper;
using Seleniumwork.PageObject;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.TestScript
{
    [TestClass]
    public class Test1
    {
        [TestMethod]
        public void TestPage()
        {

            NavigationHelper.NavigateToUrl(ObjectRepository.Config.Getwebsite());

            WebUrl url = new WebUrl();
            
            url.RegistrationPage.Click("Signin");
            url.LoginPage.Login();
            url.NavigationMenuPage.Click("Dynamic Elements");
            url.NavigationMenuPage.Click("Dropdown");
            url.SelectCountry.Select("SELECT COUNTRY");
            url.SelectCountry.Select("Ghana");

        }

    }
}
