﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Seleniumwork.ComponentHelper;
using Seleniumwork.PageObject;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.TestScript
{
    [TestClass]
    public class Test2
    {
        [TestMethod]
        public void TestPage1()
        {

            NavigationHelper.NavigateToUrl(ObjectRepository.Config.Getwebsite());

            WebUrl url = new WebUrl();

            url.RegistrationPage.Click("Signin");
            url.LoginPage.Login();
            url.NavigationMenuPage.Click("Dynamic Elements");
            url.NavigationMenuPage.Click("Dropdown");
            url.EnterCountry.Select("ENTER COUNTRY");
            url.EnterCountry.EnterCountryName("Ghana");

        }

    }
}
