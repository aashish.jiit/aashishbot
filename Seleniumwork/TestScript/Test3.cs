﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Seleniumwork.ComponentHelper;
using Seleniumwork.PageObject;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.TestScript
{
    [TestClass]
    public class Test3
    {
        [TestMethod]
        public void TestPage3()
        {

            NavigationHelper.NavigateToUrl(ObjectRepository.Config.Getwebsite());

            WebUrl url = new WebUrl();

            url.RegistrationPage.Click("Signin");
            url.LoginPage.Login();
            url.NavigationMenuPage.Click("Alert");
            url.AlertPage.Click("Simple Alert");
            url.AlertPage.Click("Click the button to display an alert box:");
            url.AlertPage.Alert.verify("I am an alert box!");




        }

    }
}
