﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Seleniumwork.ComponentHelper;
using Seleniumwork.PageObject;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.TestScript
{
    
        [TestClass]
        public class Test4
        {
            [TestMethod]
            public void TestPage4()
            {

                NavigationHelper.NavigateToUrl(ObjectRepository.Config.Getwebsite());

                WebUrl url = new WebUrl();

                url.RegistrationPage.Click("Signin");
                url.LoginPage.Login();
                url.NavigationMenuPage.Click("Alert");
                url.AlertPage.Click("Input Alert");
                url.AlertPage.Click("Click the button to demonstrate the Input box.");
                url.AlertPage.AlertForInput.EnterName("Please enter your name","Aashish Bharadwaj");
                url.AlertPage.Validate("Aashish Bharadwaj");




            }

        }
   
}
