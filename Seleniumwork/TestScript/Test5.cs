﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Seleniumwork.ComponentHelper;
using Seleniumwork.PageObject;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.TestScript
{
    [TestClass]
    public class Test5
    {
        [TestMethod]
        public void TestPage5()
        {

            NavigationHelper.NavigateToUrl(ObjectRepository.Config.Getwebsite());

            WebUrl url = new WebUrl();

            url.RegistrationPage.Click("Signin");

            url.LoginPage.Login();

            url.NavigationMenuPage.Click("Registration");
            url.RegistrationTab.Set("First Name:","Aashish");
            url.RegistrationTab.Set("Last Name:","Bharadwaj");
            url.RegistrationTab.Set("Hobby:", "Cricket");
            url.RegistrationTab.Set("Phone Number:", "9958803237");
            url.RegistrationTab.Set("Username:", "ab_automate");
            url.RegistrationTab.Set("E-mail:", "ab_automater@wahjiwah.com");
            url.RegistrationTab.Set("Password:", "*****");
            url.RegistrationTab.Set("Confirm Password:", "*****");
            url.RegistrationTab.Click("SUBMIT");
            url.RegistrationTab.VerifyLandingPage("Registration");





        }

    }
}
