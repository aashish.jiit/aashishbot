﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using Seleniumwork.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.TestScript
{
    [TestClass]
   public  class TestPageNavigation
    {
        [TestMethod]
        public void OpenPage()
        {
            INavigation page = ObjectRepository.Driver.Navigate();
            page.GoToUrl(ObjectRepository.Config.Getwebsite());
        }
    }
}
