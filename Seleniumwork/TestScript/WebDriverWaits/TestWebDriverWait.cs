﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Support.UI;

using Seleniumwork.ComponentHelper;
using Seleniumwork.Settings;
using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seleniumwork.TestScript.WebDriverWaits
{
    [TestClass]
    public class TestWebDriverWait
    {
       
        [TestMethod]
        public void TestWait()
        {
            //ObjectRepository.Driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(40));
            NavigationHelper.NavigateToUrl("https://www.udemy.com/");
        }

        [TestMethod]
        public void TestDynamicWait()
        {
            NavigationHelper.NavigateToUrl("https://www.udemy.com/");
           // ObjectRepository.Driver.Manage().Timeouts().ImplicitWait(TimeSpan.FromSeconds(1));
            //WebDriverWait wait = new WebDriverWait(ObjectRepository.Driver, TimeSpan.FromSeconds(50));
        }
    }
}
